import React from 'react'
import { Result, Button } from 'antd'

function Success({callback}) {
  return (
    <Result
      className='successPanel panel'
      status="success"
      title="Successfully uploaded!"
      subTitle="🔥 Thank you for your commission 🔥"
      extra={[
        <Button key={0} type="primary" onClick={() => {callback()}}>
          Go again! 
        </Button>,
      ]}
    />
  )
}

export default Success
