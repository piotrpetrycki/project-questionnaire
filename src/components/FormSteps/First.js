import React from 'react'

import { 
  Form, 
  Input,
  Button,
  Upload,
  message
} from "antd"
import { UploadOutlined } from '@ant-design/icons';
import { readFileAsData } from '../../utils/imageConversion';

function First({ setData, next }) {

  const fileList = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  }
  
  const parseImages = (imageList) => {
    let proms = []
    for (const file of imageList) proms.push(readFileAsData(file.originFileObj))
    
    Promise.all(proms).then(images => setData({images: images}))
  }
  
  const onFinish = (values) => { 
    parseImages(values.upload)
    
    setData({
      codename: values.codename,
      email: values.email,
      requirements: values.requirements,
    })
    next()
  }
  
  const onFinishFailed = () => message.error('Unable to proceed until errors are resolved')

  return (
    <Form
      className='stepsStep'
      name="firstStep"
      labelCol={{
        span: 5,
      }}
      wrapperCol={{
        span: 19,
      }}

      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    > 

      <Form.Item
        label="Codename"
        name="codename"
        rules={[
          {
            required: true,
            message: 'Please input the project codename!',
          },
        ]}
      >
        <Input placeholder='Poject Codename' />
      </Form.Item>

      <Form.Item
        label="Email"
        name="email"
        rules={[
          {
            type: 'email',
            message: 'Please enter a valid email'
          },
          {
            required: true,
            message: 'Please input the project codename!',
          },
        ]}
      >
        <Input placeholder='Your email'/>
      </Form.Item>

      <Form.Item
        label="Requirements"
        name="requirements"
        rules={[
          {
            required: true,
            message: 'Please provide your requirements and expectations',
          },
        ]}
      >
        <Input.TextArea rows={4} placeholder='Your requirements and expectations towards the project'/>
      </Form.Item>

      <Form.Item
        label="Examples"
        name="upload"
        valuePropName="fileList"
        getValueFromEvent={fileList}
        extra="Please, provide example images of the ui and functionalities"
        rules={[
          {
            required: true,
            message: 'Please provide example pictures',
          },
        ]}
      >
        {/* TODO: make sure each image is less than localStorage quota */}
        <Upload 
          name="images" 
          listType="picture"
          accept='image/*'
          beforeUpload={() => false}
          multiple={true}
        >
          <Button icon={<UploadOutlined />}>Click to upload</Button>
        </Upload>
      </Form.Item>

      <Form.Item
        className='stepsAction'
      >
        <Button type="primary" htmlType="submit">
          Next
        </Button>
      </Form.Item>

    </Form>
  )
}

export default First
