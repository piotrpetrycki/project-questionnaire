import React from 'react'

import { 
  Form, 
  Button,
  DatePicker,
  Slider,
  message
} from "antd"


function Third({ setData, prev, finalize }) {

  const scale = {
  8: '8k PLN',
  24: '24k PLN',
  100: '100k PLN',
};

  const onFinish = (values) => { 
    setData(values)
    finalize()
  }
  
  const onFinishFailed = () => message.error('Unable to proceed until errors are resolved')
  return (
    <Form
      className='stepsStep'
      name="firstStep"
      labelCol={{
        span: 5,
      }}
      wrapperCol={{
        span: 19,
      }}

      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    > 

      <Form.Item
        label="Time Frame"
        name="timespan"
        rules={[
          {
            required: true,
            message: 'Please provide start date and the deadline!',
          },
        ]}
      >
        <DatePicker.RangePicker renderExtraFooter={() => 'Select project start date and deadline'} />
      </Form.Item>

      <Form.Item
        label="Budget"
        name="budget"
        rules={[
          {
            required: true,
            message: 'Please provide the budget!',
          },
        ]}
      >
        <Slider marks={scale} step={1} range={{ draggableTrack: true }}  defaultValue={[8, 24]} />
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 1,
          span: 23,
        }}
      >
        <Button type="secondary" onClick={() => prev()}>
          Previous
        </Button>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>

    </Form>
  )
}

export default Third
