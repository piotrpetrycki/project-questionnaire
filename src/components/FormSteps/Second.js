import React, { useState } from 'react'

import { 
  Form, 
  Select, 
  Radio,
  Switch,
  Button, 
  message 
} from 'antd'

function Second({ setData, prev, next }) {
  const browserOptions = [
    {label: "Opera", value: "opera"},
    {label: "Firefox", value: "firefox"},
    {label: "Brave", value: "brave"},
    {label: "Chrome", value: "chrome"},
    {label: "Chromium", value: "chromium"},
    {label: "Edge", value: "edge"}
  ]

  const [darkmode, setDarkmode] = useState(true)

  const onFinish = (values) => {
    setData({darkmode: darkmode})
    setData(values)
    next()  
  };
  
  const onFinishFailed = () => message.error('Unable to proceed until errors are resolved')
  
  return (
    <Form
      className='stepsStep'
      name="secondStep"
      labelCol={{
        span: 5,
      }}
      wrapperCol={{
        span: 19,
      }}

      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >

      <Form.Item
        label="Browsers"
        name="browserList"
        rules={[
          {
            required: true,
            message: 'Please, choose at least one browser to support!',
          },
        ]}
      >
        <Select
          size="large"
          mode='tags'
          options={browserOptions}
          showArrow
        >
        </Select>
      </Form.Item>
      
      <Form.Item
        label="Technology"
        name="techStack"
        rules={[
          {
            required: true,
            message: 'Please, choose the technology stack',
          },
        ]}
      >
        <Radio.Group 
          buttonStyle="solid"
          optionType="button" 
          size="large"
          style={{ marginTop: 16 }}
        >
          <Radio.Button value="react" checked>React</Radio.Button>
          <Radio.Button value="vue">Vue</Radio.Button>
          <Radio.Button value="svelte">Svelte</Radio.Button>
          <Radio.Button value="angular" disabled>Angular</Radio.Button>
        </Radio.Group>
      </Form.Item>

      <Form.Item
        label="Dark Mode"
      >
        <Switch 
          onChange={(e) => setDarkmode(e)} 
          defaultChecked 
        />
        <span style={{marginLeft: 8, textJustify: 'center'}}>
          {
            darkmode ? "Dark mode will be included" : "No darky darky?" 
          }
        </span>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 1,
          span: 23,
        }}
      >
        <Button type="secondary" onClick={() => prev()}>
          Previous
        </Button>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  )
}

export default Second
