import React from 'react'
import { Descriptions } from 'antd'

const Entry = ({props}) => {
  const {
    date,
    codename, 
    email,
    requirements,
    images,
    browserList,
    techStack,
    darkmode,
    budget,
    timespan
  } = props

  const commisionDate = new Date(date)
  const startDate = new Date(timespan[0])
  const endDate = new Date(timespan[1])

  function retDDMMYYYY(date) {
    return `${date.getDay()}.${1 + date.getMonth()}.${date.getFullYear()}`
  }

  return(
    <Descriptions 
      className='entryItemContainer'
      title={`Data entry of user: ${email}`} 
      column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
      bordered
    >
      <Descriptions.Item label="Project Codename">{codename}</Descriptions.Item>
      <Descriptions.Item label="Commission Time">{retDDMMYYYY(commisionDate)}</Descriptions.Item>
      <Descriptions.Item label="Dark Mode">{darkmode.toString()}</Descriptions.Item>
      <Descriptions.Item label="Start Time">{retDDMMYYYY(startDate)}</Descriptions.Item>
      <Descriptions.Item label="End Time">{retDDMMYYYY(endDate)}</Descriptions.Item>
      <Descriptions.Item label="Budget">{`${budget[0]}k - ${budget[1]}k PLN`}</Descriptions.Item>
      <Descriptions.Item label="Tech Stack">{techStack}</Descriptions.Item>
      <Descriptions.Item label="Browser Support List" span={4}>{browserList.toString(' ,')}</Descriptions.Item>
      <Descriptions.Item label="Requirements" span={4}>{requirements}</Descriptions.Item>
      {
        images.map((image, index) => { return (
          <Descriptions.Item 
            key={index} 
            label={`Image ${++index}`} 
            span={4}
            >
            <img 
              className='entryItemImage'
              src={image.src} 
              alt={image.name} 
            />
          </Descriptions.Item>)
        })
      }
    </Descriptions>
  )
}


function Entries({data}) {
  return (
    <div className='entriesPanel panel'>
      {
        data.map((item, index) => {
          return <Entry key={index} props={item} />
        })
      }
    </div>
  )
}

export default Entries
