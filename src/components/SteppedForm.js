import React, { useState, useEffect } from 'react'

import FirstStep from './FormSteps/First'
import SecondStep from './FormSteps/Second'
import ThirdStep from './FormSteps/Third'

import { Steps } from 'antd'

function SteppedForm({ storage, setStorage, commisionDone }) {
  const [current, setCurrent] = useState(0)
  const [data, setData] = useState({})

  useEffect(() => {
    if ( 
      'codename' in data &&
      'email' in data &&
      'requirements' in data &&
      'images' in data &&
      'browserList' in data &&
      'techStack' in data &&
      'darkmode' in data &&
      'budget' in data &&
      'timespan' in data &&
      'date' in data
    ) {
        setStorage([...storage, data])
        commisionDone()
    }
  }, [data, storage, setStorage, commisionDone])
  
  const handleChange = (values) => setData(data => ({...data, ...values}))
  const nextStep = () => setCurrent(current + 1)
  const prevStep = () => setCurrent(current - 1)
  const finalize = () => {
    const timestamp= Date.now()

    handleChange({date: timestamp})
  }
  
  const steps = [
    {
      step: 0,
      title: 'Brief',
      description: 'General information',
      content: <FirstStep setData={handleChange} next={nextStep} />,
    },
    {
      step: 1,
      title: 'Details',
      description: 'Technical details',
      content: <SecondStep setData={handleChange} prev={prevStep} next={nextStep} />,
    },
    {
      step: 2,
      title: 'Finance & Time',
      description: 'Timeframe and budget',
      content: <ThirdStep setData={handleChange} prev={prevStep} finalize={() => finalize()}/>,
    }
  ]
  
  return (
    <div className="stepsPanel panel">
      <Steps current={current} responsive="true" >
        {steps.map((item => 
          <Steps.Step key={item.title} title={item.title} description={item.description} />
        ))}
      </Steps>
      
      <div className="stepsContainer">
          { steps.map((item) => (
            <div key={item.title} className={item.step !== current ? "hidden" : ""}>
              {item.content}
            </div>
          ))}
      </div>
    </div>
  )
}

export default SteppedForm
