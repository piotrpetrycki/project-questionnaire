export function readFileAsData(file) {
  return new Promise(function(resolve){
    let fileReader = new FileReader()

    fileReader.onload = () => resolve(
      {
        src: fileReader.result, 
        name: file.name,
        type: file.type,
        size: file.size
      }
    )

    fileReader.readAsDataURL(file)
  })
}