import { useState } from 'react'
import { Layout } from 'antd'

import './App.css';

import SteppedForm from './components/SteppedForm'
import Success from './components/Success'
import Entries from './components/Entries'
import { useLocalStorage } from './utils/localStorage'

function App() {
  const [commissionDone, setCommissionDone] = useState(false)
  const [localStorage, setLocalStorage] = useLocalStorage("entries", [])

  return (
    <Layout className="App">
      {
        commissionDone !== true
          ? <SteppedForm 
              storage={localStorage} 
              setStorage={setLocalStorage}
              commisionDone={() => setCommissionDone(true)} 
            />
          : <Success callback={() => {setCommissionDone(false)}} />
      }
      <Entries data={localStorage} />
    </Layout>
  );
}

export default App;
